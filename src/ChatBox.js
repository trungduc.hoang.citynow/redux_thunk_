import React from 'react';

class ChatBox extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            list: ['', '', '', '', '', '', ''],
            currentText: ''
        };
    }
    renderlist() {
        return (
            this.state.list.map((u, i) =><li key = {i}> {this.state.list[i]} </li>)
        );
    }

    HandleSubmit(event) {
        const newlist = this.state.list.slice();
        newlist[0] = newlist[1];
        newlist.map((u, i) => {
            if (i != 0) newlist[i - 1] = newlist[i];return null;
        })
        newlist[6] = this.state.currentText;

        this.setState({
            list: newlist,
            currentText: ''
        });
        event.preventDefault(); //prevent a tag open link.
        //In this case: preventing Form submit refresh Page
    }

    InputChat(event) {
        this.setState({
            currentText: event.target.value
        })
    }
    render() {
        return (
            <React.Fragment>
                <div> {this.renderlist()} </div>
                <form onSubmit = {this.HandleSubmit.bind(this)}>
                    <label htmlFor="chat_content">Chat: </label>
                    <input 
                    id = "chat_content" 
                    type = "text" 
                    ref = {this.textInput} 
                    value = {this.state.currentText} 
                    onChange = {this.InputChat.bind(this)}
                    style={{width:"80%"}}
                    />
                    <input type = "submit" value = "Send"/>
                </form>
            </React.Fragment>
        )
    }
}

export default ChatBox;