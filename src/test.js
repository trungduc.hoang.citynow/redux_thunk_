import React from 'react';

////Manual Transimission
// class App extends React.Component {
//   render() {
//     return <Toolbar data="dark" />;
//   }
// }

// function Toolbar(props) {
//   return (
//     <div>
//       <ThemedButton data={props.data} />
//     </div>
//   );
// }

// class ThemedButton extends React.Component {
//   render() {
//     return <p>Transmission data: {this.props.data}</p>;
//   }
// }

// With Context
const ThemeContext = React.createContext('any');
//Create new Context with data is anything.
class App extends React.Component {
  render() {
    return (
      <ThemeContext.Provider value="myData">
        <Toolbar />
      </ThemeContext.Provider>
    );
  }
}
// A component in the middle doesn't have to pass the theme down explicitly anymore.
function Toolbar(props) {
  return (
    <div>
      <ThemedButton />
    </div>
  );
}
class ThemedButton extends React.Component {
  // Assign a contextType to read the current theme context.
  // React will find the closest theme Provider above and use its value.
  static contextType = ThemeContext;
  render() {
    return <button>TransimissionData: {this.context}</button>;
  }
}

// AnotherWay with no Context:   Inversion of Control (Pug)
//Pug_Info: TypeError: Cannot add property data_transmit, object is not extensible!
//PugPosition: this.props.data_transmit = (<button>Transimission_Data: myData</button>);
//The thing you want to render at the smallest component. 
//Now put it into a constant at the highest component.
//Then you transmit it to smallest component.
// class App extends React.Component {
//   constructor(props){
//     super(props);
//     this.props.data_transmit = (<button>Transimission_Data: myData</button>);
//   }
//   render() {
//     return (
//         <Toolbar data_transmit={this.props.data_transmit}/>
//     );
//   }
// }
// // the button is passed through Toolbar
// function Toolbar(props) {
//   return (
//     <div>
//       <ThemedButton data_transmit={this.props.data_transmit}/>
//     </div>
//   );
// }
// class ThemedButton extends React.Component {
//   constructor(props){
//     super(props);
//   }
//   render() {
//     return this.props.data_transmit;
//   }
// }

  export default App;