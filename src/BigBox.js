import React from 'react';
import ChatBox from './ChatBox';
import Clock from './Clock';
//----------------Changing fieldset
const colorGroup_1 = {
    "1" : "green",
    "2" : "red"
}
const colorGroup_2 = {
    "1" : "yellow",
    "2" : "blue"
}

class SmallBox extends React.Component
{
    constructor(props){
    super(props);
    this.state = { colorId : "1" };
    }

    handleClick(e){
        if(this.state.colorId == "1") this.setState({colorId : "2"})
        else this.setState({colorId:"1"})
    }

    render(){
        if (this.props.type == 1) return (
            <React.Fragment>
                <button onClick = {this.handleClick.bind(this)}>Change background color</button>
                <div className="Box1" style={{background:colorGroup_1[this.state.colorId]}}>
                    <ChatBox/> 
                </div>
            </React.Fragment>)
        else return(
            <React.Fragment>
                <button onClick = {this.handleClick.bind(this)}>Change background color</button>
                <div className="Box2" style={{background:colorGroup_2[this.state.colorId]}}> 
                <ChatBox/>
                </div>
            </React.Fragment>
        )
    }
}

class BigBox extends React.Component
{
    constructor(props){
        super(props);
        this.state = {
            type: "1"
        }
    }

    handleClick(e) {
        if (this.state.type == "1") this.setState({
            type: "2"
        })
        else this.setState({
            type: "1"
        })
    }

    render() {
        return(
            <fieldset style={{width: "80%"}}>
                <legend><button onClick={this.handleClick.bind(this)}>Change Color Group</button></legend>
                <SmallBox type={this.state.type}/>
            </fieldset>
        )
    }
}


function children(props){
    return(
        <div>
            {props.children}
        </div>
    )
}

function Parent(){
    return(
        <children>
            <Clock/>
            <BigBox/>
        </children>
    )
}

export default Parent;